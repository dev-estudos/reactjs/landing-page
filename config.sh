#! /bin/bash

yarn add -D prettier eslint-config-prettier eslint-plugin-prettier

yarn eslint src/**/*.js --fix

# yarn eslint src/**/*.jsx --fix
